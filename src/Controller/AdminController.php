<?php
namespace App\Controller;
use App\Entity\Anime;
use App\Entity\Manga;
use App\Form\AnimeType;
use App\Form\MangaType;
use App\Repository\AnimeRepository;
use App\Repository\MangaRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Serializer\Tests\Normalizer\ObjectWithJustStaticSetterDummy;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/administration", name="admin_index")
     */
    public function index(AnimeRepository $repoAnimes, MangaRepository $repoMangas)
    {
        $animes = $repoAnimes->findBy([], ['libelle' => 'ASC']);
        $mangas = $repoMangas->findBy([], ['libelle' => 'ASC']);
        return $this->render('admin/index.html.twig',[
            "animes" => $animes,
            "mangas" => $mangas
        ]);
    }

    /**
     * @Route("/administration/ajouterAnime", name="admin_anime_ajouter")
     */
    public function ajouterAnime(Request $request, ObjectManager $manager)
    {
        $anime = new Anime();
        $form = $this->createForm(AnimeType::class, $anime);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($anime);
            $manager->flush();
            $this->addFlash('success', 'Bien crée avec succes');
            return $this->redirectToRoute('admin_index');
        }

        return $this->render('admin/ajouterAnime.html.twig',[
            'anime' => $anime,
            'form' => $form->createView()
        ]);
    }

    /**
 * @Route("/administration/modifierAnime/{id}", name="admin_anime_modifier")
 */
    public function modifierAnime(Anime $anime,Request $request, ObjectManager $manager)
    {

        $form = $this->createForm(AnimeType::class, $anime);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->flush();
            $this->addFlash('success', 'Bien modifié avec succes');
            return $this->redirectToRoute('admin_index');
        }

        return $this->render('admin/modifierAnime.html.twig',[
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/administration/supprimerAnime/{id}", name="admin_anime_supprimer")
     */
    public function supprimerAnime(Anime $anime, ObjectManager $manager)
    {
        $manager->remove($anime);
        $manager->flush();
        $this->addFlash('success', 'Bien supprimé avec succes');
        return $this->redirectToRoute('admin_index');
    }

    /**
     * @Route("/administration/ajouterManga", name="admin_manga_ajouter")
     */
    public function ajouterManga(Request $request, ObjectManager $manager)
    {
        $manga = new Manga();
        $form = $this->createForm(MangaType::class, $manga);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($manga);
            $manager->flush();
            $this->addFlash('success', 'Bien crée avec succes');
            return $this->redirectToRoute('admin_index');
        }

        return $this->render('admin/ajouterManga.html.twig',[
            'manga' => $manga,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/administration/modifierManga/{id}", name="admin_manga_modifier")
     */
    public function modifierManga(Manga $manga, Request $request, ObjectManager $manager)
    {
        $form = $this->createForm(MangaType::class, $manga);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->flush();
            $this->addFlash('success', 'Bien modifié avec succes');
            return $this->redirectToRoute('admin_index');
        }

        return $this->render('admin/modifierManga.html.twig',[
            "manga" => $manga,
            "form" => $form->createView()
        ]);
    }


    /**
     * @Route("/administration/ajouterManga/{id}", name="admin_manga_supprimer")
     */
    public function supprimerManga(Manga $manga, ObjectManager $manager)
    {
        $manager->remove($manga);
        $manager->flush();
        $this->addFlash('success', 'Bien supprimé avec succes');
        return $this->redirectToRoute('admin_index');
    }
}