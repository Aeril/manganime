<?php

namespace App\Controller;

use App\Entity\Anime;
use App\Repository\AnimeRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AnimeController extends AbstractController
{
    /**
     * @Route("/anime", name="anime_index")
     */
    public function index(AnimeRepository $repo)
    {
        $animes = $repo->findBy([], ['libelle' => 'ASC']);
        return $this->render('anime/index.html.twig', [
            "animes" => $animes
        ]);
    }

    /**
     * @Route("/anime/{id}", name="anime_visualisation")
     */
    public function anime(Anime $anime)
    {
        return $this->render('anime/visualisation.html.twig', [
            "anime" => $anime
        ]);
    }
}
