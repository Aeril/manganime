<?php

namespace App\Controller;

use App\Repository\AnimeRepository;
use App\Repository\MangaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HasardController extends AbstractController
{
    /**
     * @Route("/hasard", name="hasard_index")
     */
    public function index(AnimeRepository $repoAnime, MangaRepository $repoManga)
    {
        $animes = $repoAnime->findAll();
        $mangas = $repoManga->findAll();

        return $this->render('hasard/index.html.twig', [
            'anime' => $animes[rand(0,sizeof($animes)-1)],
            'manga' => $mangas[rand(0,sizeof($mangas)-1)]
        ]);
    }
}
