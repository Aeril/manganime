<?php

namespace App\Controller;

use App\Repository\MangaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Manga;

class MangaController extends AbstractController
{
    /**
     * @Route("/manga", name="manga_index")
     */
    public function index(MangaRepository $repo)
    {
        $mangas = $repo->findBy([], ['libelle' => 'ASC']);
        return $this->render('manga/index.html.twig', [
            "mangas" => $mangas
        ]);
    }

    /**
     * @Route("/manga/{id}", name="manga_visualisation")
     */
    public function manga(Manga $manga)
    {
        return $this->render('manga/visualisation.html.twig', [
            "manga" => $manga
        ]);
    }
}
