<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends AbstractController
{

    /**
     * @Route("/", name="site_index")
     */
    public function index(){
        return $this->render('index/home.html.twig',[
            'title' => "Bienvenue sur Manganime",
        ]);
    }

}
