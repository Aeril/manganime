<?php

namespace App\DataFixtures;

use App\Entity\Anime;
use App\Entity\Manga;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i= 1; $i<=15 ; $i++){
            $episodesAnime = rand(1,150);
            $statusAnime = rand(0,2);
            $volumesManga = rand(1,150);
            $statusManga = rand(0,2);

            $anime = new Anime();
            $anime->setLibelle("Anime $i");
            $anime->setDescription("Description first Anime  numero $i");
            $anime->setImage("img/unknow.png");
            $anime->setEditeur("MANGANIME");
            $anime->setNbEpisode("$episodesAnime");
            $anime->setStatus("$statusAnime");

            $manga = new Manga();
            $manga->setLibelle("Manga $i");
            $manga->setDescription("Description first Manga numero $i");
            $manga->setImage("img/unknow.png");
            $manga->setEditeur("MANGANIME");
            $manga->setNbVolume("$volumesManga");
            $manga->setStatus("$statusManga");

            $manager->persist($anime);
            $manager->persist($manga);

        }
        $manager->flush();
    }
}
