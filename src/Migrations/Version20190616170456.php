<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190616170456 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE anime ALTER editeur SET DEFAULT \'img/unknow.png\'');
        $this->addSql('ALTER TABLE anime ALTER nb_episode SET DEFAULT 1');
        $this->addSql('ALTER TABLE anime ALTER status SET DEFAULT 0');
        $this->addSql('ALTER TABLE manga ALTER editeur SET DEFAULT \'img/unknow.png\'');
        $this->addSql('ALTER TABLE manga ALTER nb_volume SET DEFAULT 1');
        $this->addSql('ALTER TABLE manga ALTER status SET DEFAULT 0');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE anime ALTER editeur DROP DEFAULT');
        $this->addSql('ALTER TABLE anime ALTER nb_episode DROP DEFAULT');
        $this->addSql('ALTER TABLE anime ALTER status DROP DEFAULT');
        $this->addSql('ALTER TABLE manga ALTER editeur DROP DEFAULT');
        $this->addSql('ALTER TABLE manga ALTER nb_volume DROP DEFAULT');
        $this->addSql('ALTER TABLE manga ALTER status DROP DEFAULT');
    }
}
