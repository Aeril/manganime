<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190616171310 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE anime ALTER image SET DEFAULT \'img/unknow.png\'');
        $this->addSql('ALTER TABLE anime ALTER editeur DROP DEFAULT');
        $this->addSql('ALTER TABLE manga ALTER image SET DEFAULT \'img/unknow.png\'');
        $this->addSql('ALTER TABLE manga ALTER editeur DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE anime ALTER image DROP DEFAULT');
        $this->addSql('ALTER TABLE anime ALTER editeur SET DEFAULT \'img/unknow.png\'');
        $this->addSql('ALTER TABLE manga ALTER image DROP DEFAULT');
        $this->addSql('ALTER TABLE manga ALTER editeur SET DEFAULT \'img/unknow.png\'');
    }
}
